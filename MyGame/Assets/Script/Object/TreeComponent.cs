﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeComponent : MonoBehaviour
{
    [SerializeField] private GameObject[] go_treePieces = null;
    [SerializeField] private GameObject go_treeCenter = null;
    [SerializeField] private GameObject go_hit_effect = null;
    [SerializeField] private GameObject go_Log = null;



    [SerializeField] private CapsuleCollider parentCol = null;
    [SerializeField] private CapsuleCollider childCol = null;
    [SerializeField] private Rigidbody childRigid = null;

    [SerializeField] private float debrisDestroyTime = 0f;
    [SerializeField] private float DestroyTime = 0f;

    [SerializeField] private float force = 0f;
    [SerializeField] private GameObject go_ChildTree = null;


    [SerializeField] private string chop_sound = "";
    [SerializeField] private string falldown_sound = "";
    [SerializeField] private string log_sound = "";

    public void Chop(Vector3 pos, float angleY)
    {
        Hit(pos);

        AngleCalc(angleY);

        if (CheckTreePieces())
            return;

        FallDownTree();

    }


    private void Hit(Vector3 pos)
    {
        SoundManager.instance.PlaySE(chop_sound);

        GameObject clone = Instantiate(go_hit_effect, pos, Quaternion.Euler(Vector3.zero));
        Destroy(clone, debrisDestroyTime);
    }

    private void AngleCalc(float angleY)
    {
        if (0 <= angleY && angleY <= 70) 
            DestroyPiece(2);
        else if (70 <= angleY && angleY <= 140)
            DestroyPiece(3);
        else if (140 <= angleY && angleY <= 210)
            DestroyPiece(4);
        else if (210 <= angleY && angleY <= 280)
            DestroyPiece(0);
        else if (280 <= angleY && angleY <= 360)
            DestroyPiece(1);

    }

    private void DestroyPiece(int num)
    {
        if(go_treePieces[num].gameObject != null)
        {
            GameObject clone = Instantiate(go_hit_effect, go_treePieces[num].transform.transform.position, Quaternion.Euler(Vector3.zero));
            Destroy(clone, debrisDestroyTime);
            Destroy(go_treePieces[num].gameObject);
        }
    }

    private bool CheckTreePieces()
    {
        for (int i = 0; i < go_treePieces.Length; i++)
        {
            if (go_treePieces[i].gameObject != null)
                return true;
        }
        return false;
    }

    private void FallDownTree()
    {
        SoundManager.instance.PlaySE(falldown_sound);
        Destroy(go_treeCenter);

        parentCol.enabled = false;
        childCol.enabled = true;
        childRigid.useGravity = true;

        childRigid.AddForce(UnityEngine.Random.Range(-force, force), 0f, UnityEngine.Random.Range(-force, force));

        StartCoroutine(LogCoroutine());
    }

    IEnumerator LogCoroutine()
    {
        yield return new WaitForSeconds(DestroyTime);

        SoundManager.instance.PlaySE(log_sound);

        Instantiate(go_Log, go_ChildTree.transform.position + (go_ChildTree.transform.up * 3f), Quaternion.LookRotation(go_ChildTree.transform.up));
        Instantiate(go_Log, go_ChildTree.transform.position + (go_ChildTree.transform.up * 6f), Quaternion.LookRotation(go_ChildTree.transform.up));
        Instantiate(go_Log, go_ChildTree.transform.position + (go_ChildTree.transform.up * 9f), Quaternion.LookRotation(go_ChildTree.transform.up));

        ObjectPoolManager.instance.Spawn("Tree", gameObject.transform, 10f);

        gameObject.SetActive(false);

        Destroy(go_ChildTree.gameObject);
    }

    public Vector3 GetTreeCenterPosition()
    {
        return go_treeCenter.transform.position;
    }
}
