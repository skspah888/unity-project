﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{
    [SerializeField] int hp = 0; // 바위의 체력

    [SerializeField] float destroyTime = 0; // 파편 제거 시간

    [SerializeField] private SphereCollider col = null;

    [SerializeField] private GameObject go_rock = null;        // 일반 바위
    [SerializeField] private GameObject go_debris = null;      // 파편

    [SerializeField] private GameObject go_effect = null;

    [SerializeField] private GameObject go_rock_item = null;        // 아이템

    [SerializeField] private string strike_Sound = "";
    [SerializeField] private string destroy_Sound = "";

    // 아이템 개수
    [SerializeField] private int count = 0;

    public void Mining()
    {
        SoundManager.instance.PlaySE(strike_Sound);

        var clone = Instantiate(go_effect, col.bounds.center, Quaternion.identity);
        Destroy(clone, destroyTime);

        hp--;
        if (hp <= 0)
            Destruction();
    }

    private void Destruction()
    {
        SoundManager.instance.PlaySE(destroy_Sound);

        col.enabled = false;
        for (int i = 0; i < count; i++)
        {
            Instantiate(go_rock_item, go_rock.transform.position , Quaternion.identity);
        }

        ObjectPoolManager.instance.Spawn("Rock", gameObject.transform, destroyTime);

        gameObject.SetActive(false);
        go_debris.SetActive(true);
        Destroy(go_debris, destroyTime);
    }
}
