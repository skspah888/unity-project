﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass : MonoBehaviour
{
    [SerializeField] private int hp = 0;
    [SerializeField] private float DestroyTime = 0f;
    [SerializeField] private float force = 0f;
    [SerializeField] private GameObject go_hit_effect_prefab = null;

    [SerializeField] private Item item_leaf = null;
    [SerializeField] private int leafCount = 0;

    private Inventory theInven;
    private Rigidbody[] rigidbodys;
    private BoxCollider[] boxColliders;

    [SerializeField] private string hit_sound = "";


    // Start is called before the first frame update
    void Start()
    {
        theInven = FindObjectOfType<Inventory>();
        rigidbodys = transform.GetComponentsInChildren<Rigidbody>();
        boxColliders = transform.GetComponentsInChildren<BoxCollider>();
    }

    public void Damage()
    {
        hp--;

        Hit();

        if (hp <= 0)
        {
            Destruction();
        }
    }

    private void Hit()
    {
        SoundManager.instance.PlaySE(hit_sound);

        var clone = Instantiate(go_hit_effect_prefab, transform.position + Vector3.up, Quaternion.identity);

        Destroy(clone, DestroyTime);
    }

    private void Destruction()
    {
        theInven.AcquireItem(item_leaf, leafCount);


        for (int i = 0; i < rigidbodys.Length; i++)
        {
            rigidbodys[i].useGravity = true;
            rigidbodys[i].AddExplosionForce(force, transform.position, 1f);        // 폭발세기 위치 반경
            boxColliders[i].enabled = true;
        }

        ObjectPoolManager.instance.Spawn("Grass", gameObject.transform, 5f);

        StartCoroutine(ActivateCoroutine());
        //Destroy(this.gameObject, DestroyTime);
    }

    private IEnumerator ActivateCoroutine()
    {
        yield return new WaitForSeconds(DestroyTime);
        gameObject.SetActive(false);
    }
}
