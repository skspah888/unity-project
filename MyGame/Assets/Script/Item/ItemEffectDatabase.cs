﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemEffect
{
    public string itemName;     // 이름
    [Tooltip("HP,SP,DP,HUNGRY,THIRSTY, SATISFY만 가능함")]
    public string[] part;       // 회복부위
    public int[] num;           // 수치
}

public class ItemEffectDatabase : MonoBehaviour
{
    [SerializeField] private ItemEffect[] itemEffects = null;
    [SerializeField] private StatusController thePlayerStatus = null; 
    [SerializeField] private SlotToolTip theSlotTooltip = null;

    private const string HP = "HP", MP = "MP", SP = "SP", HUNGRY = "HUNGRY", THIRSTY = "THIRSTY", SATISFY = "SATISFY";


    public void ShowToolTip(Item item, Vector3 pos)
    {
        theSlotTooltip.ShowToolTip(item, pos);
    }

    public void HideToolTip()
    {
        theSlotTooltip.HideToolTip();
    }

    public void UseItem(Item item)
    {
        if (item.itemType == Item.ItemType.Equipment)
        {
            // 장착 [ 미구현 ]
        }
        else if (item.itemType == Item.ItemType.Used)
        {
            for (int x = 0; x < itemEffects.Length; x++)
            {
                if(itemEffects[x].itemName == item.itemName)
                {
                    for (int y = 0; y < itemEffects[x].part.Length; y++)
                    {
                        switch (itemEffects[x].part[y])
                        {
                            case HP:
                                thePlayerStatus.IncreaseHP(itemEffects[x].num[y]);
                                break;
                            case SP:
                                thePlayerStatus.IncreaseSP(itemEffects[x].num[y]);
                                break;
                            case MP:
                                thePlayerStatus.IncreaseMP(itemEffects[x].num[y]);
                                break;
                            case HUNGRY:
                                thePlayerStatus.IncreaseHungry(itemEffects[x].num[y]);
                                break;
                            case THIRSTY:
                                thePlayerStatus.IncreaseThirsty(itemEffects[x].num[y]);
                                break;
                            case SATISFY:
                                break;
                            default:
                                Debug.Log("잘못된 Status 부위를 적용시킴");
                                break;
                        }
                        Debug.Log(item.itemName + "을 사용했습니다.");
                    }
                    return;
                }
            }
            Debug.Log("ItemEffectDatabase : 일치하는 ItemName 없음");
        }
    }
}
