﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : LivingEntity
{
    public static Item currentKit;      // 설치 하려는 킷 ( 연금 테이블 )

    // 필요 매니저
    private InputManager inputManager;
    private CameraManager cameraManager;
    private UIManager uiManager;

    // 필요 컴포넌트
    [SerializeField] private Transform TPS_Camera = null;
    [SerializeField] private Transform FPS_Camera = null;

    [SerializeField] private TPSMovement TPS_Movement = null;
    [SerializeField] private FPSMovement FPS_Movement = null;
    private Animator[] playerAnimators;         // 플레이어 캐릭터의 애니메이터
    private Rigidbody myRigid;

    // 상태 변수
    private bool isGround = true;
    private bool isWalk = false;
    private bool isRun = false;
    private bool isAttack = false;
    private bool isInvincible = false;
    public static bool isSound = true;

    // 연금 테이블
    private bool isPreview = false;
    private GameObject go_preview;
    private Vector3 previewPos;
    [SerializeField] private float rangeAdd = 0f;  // 건축시 추가 사정거리
    [SerializeField] private QuickSlotController theQuickSlot = null;

    // 아이템 레이어에만 반응하도록 레이어 마스크 설정
    [SerializeField] private LayerMask layerMask = 0;

    //무적
    private float Invincibletime = 0f;
    private float playerSoundDuration = 0f;
    private float WalkSoundTime = 0.5f;
    private float RunSoundTime = 0.25f;
    private float RunNWalkSoundTime = 0f;
    private float CurrentTime = 0f;

    public float GetCurrentTime() { return CurrentTime; }

    // 스피드 대입용
    [SerializeField] private float walkSpeed = 0f;
    [SerializeField] private float runSpeed = 0f;
    private float applySpeed = 0f;       

    // 움직임 체크 변수
    private Vector3 lastPos;
    [SerializeField] private float jumpForce = 7f;

    // 캐릭터 이동 및 카메라회전
    private PlayerMovement CurrentController;

    // 땅 착지 여부
    private CapsuleCollider capsuleCollider;

    // 공격 데미지
    public bool isSwing { get; protected set; } // 사망 상태

    public void SetAttack(bool isValue) { isAttack = isValue; }

    public void AnimatorSetBool(string name, bool isValue)
    {
        foreach (var anim in playerAnimators)
        {
            anim.SetBool(name, isValue);
        }
    }

    public void AnimatorSetTrigger(string name)
    {
        foreach (var anim in playerAnimators)
        {
            anim.SetTrigger(name);
        }
    }


    private void Start()
    {
        cameraManager = CameraManager.instance;
        inputManager = InputManager.instance;
        uiManager = UIManager.instance;
        capsuleCollider = GetComponent<CapsuleCollider>();
        myRigid = GetComponent<Rigidbody>();
        playerAnimators = GetComponentsInChildren<Animator>();
        CurrentController = TPS_Movement;
        applySpeed = walkSpeed;
        RunNWalkSoundTime = WalkSoundTime;
        isSwing = false;
    }

    // 체력 회복
    public override void RestoreHealth(float newHealth)
    {
        // 플레이어는 일반 몬스터와 다르게 StatusController에서 HP를 관리함
        if (dead)
        {
            // 이미 사망한 경우 체력을 회복할 수 없음
            return;
        }

        // 체력 추가
        // 갱신된 체력으로 체력 Status 갱신
        uiManager.IncreaseHP((int)newHealth);
    }

    // 데미지 처리 : isDuration이 true면 캐릭터의 Duration을 사용하고, false면 공격하는 주체의 Duration을 사용한다.
    // => 몬스터같은 경우는 여럿이서 공격을하지 isDuration을 true로 넘겨준다.
    public override void OnDamage(float damage, Vector3 hitPoint, Vector3 hitDirection, bool isDuration = false)
    {
        if (!dead)
        {
            if(isDuration)
            {
                // 무적상태가 아니면 데미지 받음
                if (!isInvincible)
                {
                    // 갱신된 체력으로 체력 Status 갱신
                    uiManager.DecreaseHP((int)damage);
                    isInvincible = true;

                    // 공격중이면 그냥 공격 계속 하게함
                    if (!isAttack)
                    {
                        // 애니메이션 변경
                        AnimatorSetTrigger("Hit");
                        isAttack = false;
                    }
                }


            }
            else // 무적시간과 상관없이 그냥 데미지 받음
            {
                // 공격중이면 그냥 공격 계속 하게함
                if (!isAttack)
                {
                    // 애니메이션 변경
                    AnimatorSetTrigger("Hit");
                    isAttack = false;
                }
                uiManager.DecreaseHP((int)damage);
            }

            if (playerSoundDuration > 1f)
            {
                SoundManager.instance.PlaySE("Player_Hurt");
                playerSoundDuration = 0f;
            }
        }

        

        // 체력이 0 이하 && 아직 죽지 않았다면 사망 처리 실행
        if (uiManager.GetCurrentHP() <= 0 && !dead)
        {
            Die();
        }
       
    }

    // 사망 처리
    public override void Die()
    {
        // LivingEntity의 Die() 실행(사망 적용)
        base.Die();

        //AnimatorSetTrigger("Die");
    }

    private void Update()
    {
        IsGround();

        if (GameManager.canPlayerMove)
        {
            TryAttack();
            TryJump();
            TryRun();
            TryMove();
            CurrentController.Rotate(applySpeed);
        }

        ElapseTime();
        ResetError();
    }

    private void ElapseTime()
    {
        playerSoundDuration += Time.deltaTime;
        if(!isSound)
            CurrentTime += Time.deltaTime;

        if (CurrentTime > RunNWalkSoundTime)
        {
            isSound = true;
            CurrentTime = 0f;
        }


        // 무적시간 1초 계산
        if (isInvincible)
        {
            Invincibletime += Time.deltaTime;
            if(Invincibletime > 1f)
            {
                Invincibletime = 0f;
                isInvincible = false;
            }
        }
    }

    private void ResetError()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            cameraManager.FPSCameraOn();
            CurrentController = FPS_Movement;
            UIManager.instance.SetGameObject(true);
        }

        if (Input.GetKeyDown(KeyCode.F2))
        {
            cameraManager.TPSCameraOn();
            CurrentController = TPS_Movement;
            UIManager.instance.SetGameObject(false);
        }

        // 공격프리징 현상
        if (Input.GetKeyDown(KeyCode.M))
        {
            isAttack = false;
        }
    }

    private void TryAttack()
    {
        //if (CraftManual.isActivated || CraftManual.isPreviewActivated)
        //    return;

        if(currentKit == null)
        {
            // 마우스 클릭했을때 공격,
            if (Input.GetButtonDown("Fire1"))
            {
                if (!isAttack)
                {
                    isAttack = true;
                    AnimatorSetTrigger("Attack1");      // 첫번째 공격 모션 활성화
                    uiManager.ComboUIActivate(true);    // Combo UI 띄우기
                    uiManager.UIUpdate();               // Combo Update실행 [ 코루틴 실행 ]
                }
            }
        }
        else
        {
            if (!isPreview)
                InstallPreviewKit();
            PreviewPositionUpdate();
            Build();
        }
       
    }

    private void InstallPreviewKit()
    {
        isPreview = true;

        if (CameraManager.instance.IsTPS)   // 3인칭 일때
        {
            go_preview = Instantiate(currentKit.kitPreviewPrefab, myRigid.transform.position + myRigid.transform.forward * 2f, Quaternion.identity);
        }
        else // 1인칭일때 
        {
            go_preview = Instantiate(currentKit.kitPreviewPrefab, FPS_Camera.position + FPS_Camera.forward * 2f, Quaternion.identity);
        }
    }

    private void PreviewPositionUpdate()
    {
        // 3인칭일때 -> CameraArm따라가게함?
        if (CameraManager.instance.IsTPS)
        {
            if (Physics.Raycast(myRigid.transform.position + myRigid.transform.up, myRigid.transform.forward + (-myRigid.transform.up) * 0.3f, out hitInfo, rangeAdd, layerMask))
            {
                previewPos = hitInfo.point;
                go_preview.transform.position = previewPos;
            }
        }
        else // 1인칭일때 -> CrossHair?
        {
            if (Physics.Raycast(FPS_Camera.position, FPS_Camera.forward, out hitInfo, rangeAdd, layerMask))
            {
                previewPos = hitInfo.point;
                go_preview.transform.position = previewPos;
            }
        }
    }

    private void Build()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            if(go_preview.GetComponent<PreviewObject>().isBuildable())
            {
                theQuickSlot.DecreaseSelectedItem();
                GameObject temp = Instantiate(currentKit.kitPrefab, previewPos, Quaternion.identity);
                temp.name = currentKit.itemName;
                Destroy(go_preview);
                currentKit = null;
                isPreview = false;
            }
        }
    }

    public void Cancel()
    {
        if (isPreview)
        {
            Destroy(go_preview);
            currentKit = null;
            isPreview = false;
        }
    }

    private void IsGround()
    {
        // 캡슐 y크기의 반만큼 Ray를 down 방향으로 쏜다. [ 오차를 생각하여 여유분 0.1f ]
        isGround = Physics.Raycast(transform.position + Vector3.up * 0.9f, Vector3.down, capsuleCollider.bounds.extents.y + 0.1f);
            
    }

    private void TryJump()
    {
        // 땅에 있는 상태랑 공격상태가 아닐때 점프 가능
        if (Input.GetKeyDown(KeyCode.Space) && isGround && !isAttack && UIManager.instance.GetCurrentSP() > 0)
        {
            Jump();
        }
    }

    private void Jump()
    {
        myRigid.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        //myRigid.velocity = transform.up * jumpForce;
        AnimatorSetBool("Jump", true);
        UIManager.instance.JumpAnimation(true);
        UIManager.instance.DecreaseStamina(150);
        SoundManager.instance.PlaySE("Player_Jump");
    }

    IEnumerator JumpCoroutine()
    {
        yield return new WaitForSeconds(0.8f);

        UIManager.instance.JumpAnimation(false);
        AnimatorSetBool("Jump", false);
    }

    private void TryRun()
    {
        // 계속 누르고 있는 상태
        if (Input.GetKey(KeyCode.LeftShift) && UIManager.instance.GetCurrentSP() > 0)
        {
            Running();
        }
        if (Input.GetKeyUp(KeyCode.LeftShift) || UIManager.instance.GetCurrentSP() <= 0)
        {
            RunningCancle();
        }
    }

    private void Running()
    {
        isRun = true;
        AnimatorSetBool("Run", isRun);
        UIManager.instance.RunningAnimation(isRun);
        applySpeed = runSpeed;
        RunNWalkSoundTime = RunSoundTime;
        UIManager.instance.DecreaseStamina(1);
    }

    private void RunningCancle()
    {
        isRun = false;
        RunNWalkSoundTime = WalkSoundTime;
        AnimatorSetBool("Run", isRun);
        UIManager.instance.RunningAnimation(isRun);
        applySpeed = walkSpeed;
    }


    private void TryMove()
    {
        // 공격 상태일때는 움직이지 않음
        if (!isAttack)
        {
            CurrentController.Move(applySpeed);
        }
    }


    // 충돌체 저장용
    private RaycastHit hitInfo;

    public void InteractObject()
    {
        if(CheckObject())
        {
            if(hitInfo.transform.tag == "Rock")
            {
                hitInfo.transform.GetComponent<Rock>().Mining();
            }
            else if (hitInfo.transform.tag == "Twig")
            {
                hitInfo.transform.GetComponent<Twig>().Damage(myRigid.transform);
            }
            else if (hitInfo.transform.tag == "Grass")
            {
                hitInfo.transform.GetComponent<Grass>().Damage();
            }
            else if (hitInfo.transform.tag == "Tree")
            {
                if(!cameraManager.IsTPS)
                    StartCoroutine(FPS_Movement.TreeLookCoroutine(hitInfo.transform.GetComponent<TreeComponent>().GetTreeCenterPosition()));
                hitInfo.transform.GetComponent<TreeComponent>().Chop(hitInfo.point, myRigid.transform.eulerAngles.y);
            }
        }
       
    }

    private bool CheckObject()
    {
        // 3인칭 시점
        if (cameraManager.IsTPS)
        {
            //if (Physics.SphereCast(transform.position, transform.lossyScale.y, transform.forward, out hitInfo, 1f))
            //if (Physics.SphereCast(transform.position + transform.up, GetComponent<CapsuleCollider>().bounds.extents.y + 0.1f, transform.forward + (-transform.up) * 0.3f, out hitInfo, 0.5f))
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hitInfo, 5f))
            {
                return true;
            }
            else
                return false;
        }

        // 1인칭 시점
        else
        {
            if (Physics.Raycast(FPS_Camera.position, FPS_Camera.TransformDirection(Vector3.forward), out hitInfo, 5f))
            {
                return true;
            }
            else
                return false;
        }
    }
}
