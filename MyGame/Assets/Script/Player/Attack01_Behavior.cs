﻿using UnityEngine;

public class Attack01_Behavior : StateMachineBehaviour
{
    private PlayerController playerController;
    private UIManager uiManager;

    static bool IsCheck = false;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        uiManager = UIManager.instance;
        playerController = FindObjectOfType<PlayerController>();
        IsCheck = false;
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // 슬라이더바 업데이트
        //uiManager.UIUpdate();

        //if (uiManager.GetIsCombo())  // 콤보가 성공했다 => 다음 콤보 이동
        //{
        //    playerController.AnimatorSetTrigger("Attack2");
        //}
        //else                        // 콤보가 성공하지 못했다 => Idle로 이동
        //{
        //    playerController.SetAttack(false);
        //}
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!IsCheck)
        {
            if (!uiManager.GetIsCombo())
                playerController.SetAttack(false);
            //else
            //    playerController.AnimatorSetTrigger("Attack2");

            playerController.InteractObject();
            IsCheck = true;
            SoundManager.instance.PlaySE("Player_Attack");
        }
    }

    public override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // MonoBehaviour.OnAnimatorMove 직후에 실행
    }

    public override void OnStateMachineExit(Animator animator, int stateMachinePathHash)
    {
        // 스크립트가 부착된 상태 기계에서 빠져나올때 실행
    }
}