﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 5f;                // 앞뒤 움직임의 속도
    public float rotateSpeed = 180f;            // 좌우 회전 속도
    public float lookSensitivity;               // 마우스 민감도
    public float cameraRotationLimit = 45;       // 카메라 한계

    protected InputManager playerInput;           // 플레이어 입력을 알려주는 컴포넌트
    protected Rigidbody playerRigidbody;          // 플레이어 캐릭터의 리지드바디
    protected Animator[] playerAnimators;         // 플레이어 캐릭터의 애니메이터

    protected virtual void Start()
    {
        playerInput = InputManager.instance;
        playerAnimators = GetComponentsInChildren<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
    }

    public virtual void Move(float speed) { }
    public virtual void Rotate(float speed) { }

    protected void AnimatorSetBool(string name, bool isValue)
    {
        foreach (var anim in playerAnimators)
        {
            anim.SetBool(name, isValue);
        }
    }

    protected void AnimatorSetTrigger(string name)
    {
        foreach (var anim in playerAnimators)
        {
            anim.SetTrigger(name);
        }
    }

}
