﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSMovement : PlayerMovement
{
    // 필요 컴포넌트
    [SerializeField] private Camera theCamera = null;

    // 카메라 한계
    private float currentCameraRotationX;

    public override void Move(float speed)
    {
        Vector3 moveInput = new Vector2(playerInput.MoveX, playerInput.MoveY);
        bool isMove = moveInput.magnitude != 0;

        if (isMove)
        {
            Vector3 _moveHorizontal = transform.right * moveInput.x;
            Vector3 _moveVertical = transform.forward * moveInput.y;

            Vector3 _velocity = (_moveHorizontal + _moveVertical).normalized * speed;

            playerRigidbody.MovePosition(transform.position + _velocity * Time.deltaTime);

            //float _moveDirX = Input.GetAxisRaw("Horizontal");
            //float _moveDirZ = Input.GetAxisRaw("Vertical");

            //Vector3 _moveHorizontal = transform.right * _moveDirX;
            //Vector3 _moveVertical = transform.forward * _moveDirZ;

            //Vector3 _velocity = (_moveHorizontal + _moveVertical).normalized * moveSpeed;

            //playerRigidbody.MovePosition(transform.position + _velocity * Time.deltaTime);

            if (PlayerController.isSound)
            {
                SoundManager.instance.PlaySE("Player_Run");
                PlayerController.isSound = false;
            }
        }

        base.AnimatorSetBool("Walk", isMove);
        UIManager.instance.WalkingAnimation(isMove);
    }

    public override void Rotate(float speed)
    {
        CharaterRotation();
        CameraRotation();
    }

    private void CharaterRotation()
    {
        // 좌우 캐릭터 회전
        float _yRotation = Input.GetAxis("Mouse X");
        Vector3 _characterRotationY = new Vector3(0f, _yRotation, 0f) * lookSensitivity;
        playerRigidbody.MoveRotation(playerRigidbody.rotation * Quaternion.Euler(_characterRotationY));
    }

    public void CameraRotation()
    {
        if (!pauseCameraRoatation)
        {
            // 상하 카메라 회전
            // Raw는 딱 떨어지게 반환
            float _xRotation = Input.GetAxis("Mouse Y");
            float _cameraRotationX = _xRotation * lookSensitivity;
            currentCameraRotationX -= _cameraRotationX;
            currentCameraRotationX = Mathf.Clamp(currentCameraRotationX, -cameraRotationLimit, cameraRotationLimit);

            theCamera.transform.localEulerAngles = new Vector3(currentCameraRotationX, 0f, 0f);
        }
    }

    private bool pauseCameraRoatation = false;

    public IEnumerator TreeLookCoroutine(Vector3 target)
    {
        pauseCameraRoatation = true;

        Quaternion direction = Quaternion.LookRotation(target - theCamera.transform.position);
        Vector3 eulerValue = direction.eulerAngles;
        float destinationX = eulerValue.x;

        while(Mathf.Abs(destinationX - currentCameraRotationX) >= 0.5f)
        {
            eulerValue = Quaternion.Lerp(theCamera.transform.localRotation, direction, 0.3f).eulerAngles;
            theCamera.transform.localRotation = Quaternion.Euler(eulerValue.x, 0f, 0f);
            currentCameraRotationX = theCamera.transform.localEulerAngles.x;

            yield return null;
        }

        pauseCameraRoatation = false;
    }

}
