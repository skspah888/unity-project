﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPSMovement : PlayerMovement
{
    float CameraRotationX = 0f;
    float CameraRotationY = 0f;

    // 필요 컴포넌트
    [SerializeField] private Camera theCamera = null;
    [SerializeField] private GameObject CameraArm = null;


    public override void Rotate(float speed)
    {
        float YRotation = Input.GetAxis("Mouse X");
        float XRotation = Input.GetAxis("Mouse Y");

        CameraRotationX -= XRotation * lookSensitivity;
        CameraRotationY += YRotation * lookSensitivity;

        CameraRotationX = Mathf.Clamp(CameraRotationX, -cameraRotationLimit, cameraRotationLimit);

        CameraArm.transform.rotation = Quaternion.Euler(CameraArm.transform.rotation.x + CameraRotationX, CameraArm.transform.rotation.y + CameraRotationY, 0);
    }


    public override void Move(float speed)
    {
        float moveDirX = Input.GetAxisRaw("Horizontal");
        float moveDirZ = Input.GetAxisRaw("Vertical");

        bool isMove = !(moveDirZ == 0 && moveDirX == 0);

        if (isMove)
        {
            playerRigidbody.MovePosition(transform.position + transform.forward * Time.deltaTime * speed);
            if (PlayerController.isSound)
            {
                SoundManager.instance.PlaySE("Player_Run");
                PlayerController.isSound = false;
            }
        }

        Quaternion Camera_rotation = theCamera.transform.rotation;
        Camera_rotation.x = 0;
        Camera_rotation.z = 0;

        if(moveDirZ == 1 && moveDirX == 0)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Camera_rotation * Quaternion.Euler(0, 0, 0), rotateSpeed * Time.deltaTime);
        }
        else if (moveDirZ == -1 && moveDirX == 0)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Camera_rotation * Quaternion.Euler(0, 180, 0), rotateSpeed * Time.deltaTime);
        }
        else if (moveDirX == 1 && moveDirZ == 0)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Camera_rotation * Quaternion.Euler(0, 90, 0), rotateSpeed * Time.deltaTime);
        }
        else if (moveDirX == -1 && moveDirZ == 0)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Camera_rotation * Quaternion.Euler(0, 270, 0), rotateSpeed * Time.deltaTime);
        }

        if(moveDirZ == 1 && moveDirX == 1)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Camera_rotation * Quaternion.Euler(0, 45, 0), rotateSpeed * Time.deltaTime);
        }
        else if (moveDirZ == 1 && moveDirX == -1)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Camera_rotation * Quaternion.Euler(0, 315, 0), rotateSpeed * Time.deltaTime);
        }
        else if (moveDirZ == -1 && moveDirX == 1)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Camera_rotation * Quaternion.Euler(0, 135, 0), rotateSpeed * Time.deltaTime);
        }
        else if (moveDirZ == -1 && moveDirX == -1)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Camera_rotation * Quaternion.Euler(0, 225, 0), rotateSpeed * Time.deltaTime);
        }

        base.AnimatorSetBool("Walk", isMove);
    }
}
