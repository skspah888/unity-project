﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class ObjectPrefab
{
    public string prefabName;           // 생성할 이름 [ 찾기 및 등록용 ]
    public GameObject go_Prefab;        // 생성할 프리팹
    public int buffAmount;              // 미리 만들 개수
}


public class ObjectPoolManager : MonoBehaviour
{
    public static ObjectPoolManager instance
    {
        get
        {
            // 만약 싱글톤 변수에 아직 오브젝트가 할당되지 않았다면
            if (m_instance == null)
            {
                // 씬에서 GameManager 오브젝트를 찾아 할당
                m_instance = FindObjectOfType<ObjectPoolManager>();
            }

            // 싱글톤 오브젝트를 반환
            return m_instance;
        }
    }

    private static ObjectPoolManager m_instance;              // 싱글톤이 할당될 static 변수

    private void Awake()
    {
        if (m_instance == null)
        {
            m_instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    [SerializeField] private ObjectPrefab[] PrefabArray;
    private Dictionary<string, List<GameObject>> poolDictionary;

    private void Start()
    {
        poolDictionary = new Dictionary<string, List<GameObject>>();

        // 미리 만들어 놓을 개수
        for (int i = 0; i < PrefabArray.Length; i++)
        {
            var gameObjectList = new List<GameObject>();
            var prefab = PrefabArray[i].go_Prefab;
            var name = PrefabArray[i].prefabName;
            var buffAmoumt = PrefabArray[i].buffAmount;

            for (int j = 0; j < buffAmoumt; j++)
            {
                var go = Instantiate(prefab);
                go.SetActive(false);
                go.transform.position = Vector3.zero;
                gameObjectList.Add(go);
            }
            poolDictionary.Add(name, gameObjectList);
        }
    }

    public GameObject Spawn(string gameObjectName, Transform originTransform, float SpawnTime = 0.1f)
    {
        var goList = poolDictionary[gameObjectName];
        var go = goList.FirstOrDefault(g => g.activeInHierarchy == false);

        // 오브젝트가 있음
        if (go != null)
        {
            StartCoroutine(DeActivateSpawnCoroutine(go, originTransform, SpawnTime));
            return go;
        }

        // 오브젝트가 없음
        else
        {
            StartCoroutine(ActivateSpawnCoroutine(go, gameObjectName, originTransform, SpawnTime));
            return go;
        }
    }

    public IEnumerator DeActivateSpawnCoroutine(GameObject go ,Transform _transform, float SpawnTime)
    {
        Transform originTransform = _transform;
        yield return new WaitForSeconds(SpawnTime);

        go.SetActive(true);
        go.transform.position = originTransform.localPosition;
        go.transform.rotation = originTransform.localRotation;

        var r = go.GetComponent<Rigidbody>();
        if (r != null)
        {
            r.velocity = Vector3.zero;
            r.angularVelocity = Vector3.zero;
        }
    }

    public IEnumerator ActivateSpawnCoroutine(GameObject go , string gameObjectName, Transform _transform, float SpawnTime)
    {
        Transform originTransform = _transform;
        yield return new WaitForSeconds(SpawnTime);

        var goList = poolDictionary[gameObjectName];

        var prefab = PrefabArray.FirstOrDefault(g => g.prefabName == gameObjectName);
        go = Instantiate(prefab.go_Prefab, originTransform.localPosition, originTransform.localRotation);
        goList.Add(go);

        var r = go.GetComponent<Rigidbody>();
        if (r != null)
        {
            r.velocity = Vector3.zero;
            r.angularVelocity = Vector3.zero;
        }

    }
}
