﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 게임오브젝트에 안붙여도 됨
[CreateAssetMenu(fileName = "New Item", menuName = "New Item/item")]
public class Item : ScriptableObject
{
    public string itemName;         // 아이템 이름
    [TextArea]
    public string itemDesc;         // 아이템 설명
    public ItemType itemType;       // 아이템 유형
    public Sprite itemImage;        // 아이템 이미지  [ Sprite는 Canvas가 필요없음 ]
    public GameObject itemPrefab;   // 아이템 프리팹

    public string weaponType;       // 무기 유형

    public GameObject kitPrefab;
    public GameObject kitPreviewPrefab;

    public enum ItemType
    {
        Equipment,      // 장비 아이템
        Used,           // 소모품
        Ingredient,     // 재료
        Kit,            // 키트
        ETC             // 기타
    }



}
