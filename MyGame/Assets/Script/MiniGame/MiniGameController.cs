﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameController : MonoBehaviour
{
    private Rigidbody playerRigidbody;       // 이동에 사용할 리지드바디 컴포넌트
    public float speed = 8f;               // 이동 속력

    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // 수평축과 수직축의 입력값을 감지하여 저장
        float xInput = Input.GetAxis("Horizontal");
        float zInput = Input.GetAxis("Vertical");

        // 실제 이동 속도를 입력값과 이동 속력을 사용해 결정
        float xSpeed = xInput * speed;
        float zSpeed = zInput * speed;

        // Vector3 속도를 (xSpeed, 0 , zSpeed)로 생성
        Vector3 newVelocity = new Vector3(xSpeed, 0f, zSpeed);

        // 리지드바디의 속도에 newVelocity 할당 [ Addforce()는 관성 적용, velocity에 직접 할당은 관성 무시 ]
        playerRigidbody.velocity = newVelocity;


        //if (Input.GetKey(KeyCode.UpArrow))
        //{
        //    playerRigidbody.AddForce(0f, 0f, speed);
        //}

        //if (Input.GetKey(KeyCode.DownArrow))
        //{
        //    playerRigidbody.AddForce(0f, 0f, -speed);
        //}

        //if (Input.GetKey(KeyCode.RightArrow))
        //{
        //    playerRigidbody.AddForce(speed, 0f, 0f);
        //}

        //if (Input.GetKey(KeyCode.LeftArrow))
        //{
        //    playerRigidbody.AddForce(-speed, 0f, 0f);
        //}
    }

    public void Die()
    {
        gameObject.SetActive(false);

        // 씬에 존재하는 GameManager 타입의 오브젝트를 찾아서 가져오기
        MiniGameManager gameManager = FindObjectOfType<MiniGameManager>();
        gameManager.EndGame();
        
    }
}
