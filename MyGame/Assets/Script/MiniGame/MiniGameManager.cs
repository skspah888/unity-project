﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MiniGameManager : MonoBehaviour
{
    public GameObject gameoverText;     // 게임 오버 시 활성화할 텍스트 
    public Text timeText;               // 생존 시간을 표시할 텍스트 컴포넌트 
    public Text recordText;             // 최고 기록을 표시할 텍스트 컴포넌트 

    private float surviveTime;           // 생존 시간 
    private bool isGameover;             // 게임 오버 상태 

    void Start()
    {
        surviveTime = 0f;
        isGameover = false;

        // Scene 활성화 해주기
        SceneManager.SetActiveScene(gameObject.scene);
    }

    void Update()
    {
        if (!isGameover)
        {
            surviveTime += Time.deltaTime;

            timeText.text = "Time : " + (int)surviveTime;
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                //SceneManager.UnloadSceneAsync("MiniGame");
                //SceneManager.SetActiveScene(SceneManager.GetSceneByName("MiniGame"));

                //StartCoroutine(LoadCoroutine());
                GameManager gm = FindObjectOfType<GameManager>();
                gm.ActivateAllObjects();

                SceneManager.UnloadSceneAsync("MiniGame");
            }
        }
    }

    IEnumerator LoadCoroutine()
    {
        yield return null;
        //AsyncOperation operation = SceneManager.UnloadSceneAsync("MiniGame");

        //// 씬 동기화
        //while (!operation.isDone)
        //{
        //    yield return null;
        //}

        //theSaveNLoad = FindObjectOfType<SaveNLoad>();
        //theSaveNLoad.LoadGame();
        //gameObject.SetActive(false);
    }

    // 현재 게임을 게임오버 상태로 변경하는 메서드 
    public void EndGame()
    {
        isGameover = true;
        gameoverText.SetActive(true);

        // BestTime 키로 저장된 이전까지의 최고 기록 가져오기
        float bestTime = PlayerPrefs.GetFloat("BestTime");

        // 이전까지의 최고 기록보다 현재 생존 시간이 더 크다면
        if (surviveTime > bestTime)
        {
            // 갱신 및 저장
            bestTime = surviveTime;
            PlayerPrefs.SetFloat("BestTime", bestTime);
        }

        recordText.text = "Best Time : " + (int)bestTime;
    }

    public void ExitGame()
    {
        GameManager gm = FindObjectOfType<GameManager>();
        gm.ActivateAllObjects();

        SceneManager.UnloadSceneAsync("MiniGame");
    }
}
