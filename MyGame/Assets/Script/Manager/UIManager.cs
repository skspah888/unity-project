﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance
    {
        get
        {
            if (m_instance == null)
            {
                m_instance = FindObjectOfType<UIManager>();
            }

            return m_instance;
        }
    }
    private static UIManager m_instance; // 싱글톤이 할당될 변수

    void Awake()
    {
        if (m_instance == null)
        {
            m_instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    [SerializeField] private Combo ComboUI = null;
    [SerializeField] private StatusController StatusUI = null;
    [SerializeField] private Crosshair CrosshairUI = null;

    public void IncreaseHP(int _count)
    {
        StatusUI.IncreaseHP(_count);
    }

    public void DecreaseHP(int _count)
    {
        StatusUI.DecreaseHP(_count);
    }

    public void IncreaseMP(int _count)
    {
        StatusUI.IncreaseMP(_count);
    }

    public void DecreaseMP(int _count)
    {
        StatusUI.DecreaseMP(_count);
    }

    public void IncreaseHungry(int _count)
    {
        StatusUI.IncreaseHungry(_count);
    }

    public void DecreaseHungry(int _count)
    {
        StatusUI.DecreaseHungry(_count);
    }

    public void IncreaseThirsty(int _count)
    {
        StatusUI.IncreaseThirsty(_count);
    }

    public void DecreaseThirsty(int _count)
    {
        StatusUI.DecreaseThirsty(_count);
    }

    public void IncreaseSatisfy(int _count)
    {
        StatusUI.IncreaseSatisfy(_count);
    }

    public void DecreaseSatisfy(int _count)
    {
        StatusUI.DecreaseSatisfy(_count);
    }

    public void DecreaseStamina(int _count)
    {
        StatusUI.DecreaseStamina(_count);
    }

    public int GetCurrentSP()
    {
        return StatusUI.GetCurrentSP(); 
    }

    public int GetCurrentHP()
    {
        return StatusUI.GetCurrentHP();
    }

    public int GetCurrentMP()
    {
        return StatusUI.GetCurrentMP();
    }

    public void WalkingAnimation(bool _flag)
    {
        CrosshairUI.WalkingAnimation(_flag);
    }

    public void RunningAnimation(bool _flag)
    {
        CrosshairUI.RunningAnimation(_flag);
    }

    public void JumpAnimation(bool _flag)
    {
        CrosshairUI.JumpAnimation(_flag);
    }

    public void CrouchingAnimation(bool _flag)
    {
        CrosshairUI.CrouchingAnimation(_flag);
    }

    public void FineSightAnimation(bool _flag)
    {
        CrosshairUI.FineSightAnimation(_flag);
    }

    public void AttackAnimation()
    {
        CrosshairUI.AttackAnimation();
    }

    public void SetGameObject(bool isValue)
    {
        CrosshairUI.SetGameObject(isValue);
    }

    public void ResetUI()
    {
        ComboUI.ResetUI();
    }

    public void ComboUIActivate(bool isActivate)
    {
        ComboUI.ComboUIActivate(isActivate);
    }

    public void UIUpdate()
    {
        if(ComboUI.isActiveAndEnabled)
            ComboUI.UIUpdate();
    }

    public bool GetIsCombo() { return ComboUI.GetIsCombo(); }

    public void ComboAttack()
    {
        ComboUI.SetAttack();
    }


}
