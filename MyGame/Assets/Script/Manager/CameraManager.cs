﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField] private Camera TPSCamara = null;
    [SerializeField] private Camera FPSCamera = null;

    

    public static CameraManager instance
    {
        get
        {
            // 만약 싱글톤 변수에 아직 오브젝트가 할당되지 않았다면
            if (m_instance == null)
            {
                // 씬에서 GameManager 오브젝트를 찾아 할당
                m_instance = FindObjectOfType<CameraManager>();
            }

            // 싱글톤 오브젝트를 반환
            return m_instance;
        }
    }

    private static CameraManager m_instance;

    public bool IsTPS = true;

    void Awake()
    {
        if (m_instance == null)
        {
            m_instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    void Start()
    {
        TPSCameraOn();
    }

    public void TPSCameraOn()
    {
        TPSCamara.enabled = true;
        FPSCamera.enabled = false;
        IsTPS = true;
    }

    public void FPSCameraOn()
    {
        FPSCamera.enabled = true;
        TPSCamara.enabled = false;
        IsTPS = false;
    }

}
