﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance
    {
        get
        {
            // 만약 싱글톤 변수에 아직 오브젝트가 할당되지 않았다면
            if (m_instance == null)
            {
                // 씬에서 GameManager 오브젝트를 찾아 할당
                m_instance = FindObjectOfType<GameManager>();
            }

            // 싱글톤 오브젝트를 반환
            return m_instance;
        }
    }

    private static GameManager m_instance;              // 싱글톤이 할당될 static 변수
    public bool isGameover { get; private set; }        // 게임 오버 상태

    public GameObject[] GameObjects;

    private void Awake()
    {
        if (m_instance == null)
        {
            m_instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    public static bool canPlayerMove = true;            // 플레이어 움직임 제어
    public static bool isOpenInventory = false;         // 인벤토리 활성화 여부
    public static bool isOpenCraftManual = false;         // 건축 메뉴얼 활성화
    public static bool isOpenComputer = false;         // 컴퓨터 활성화
    public static bool isOpenArchemyTable = false;      // 연금 테이블 창 활성화

    public static bool isPause = false; // 메뉴 호출되면 true

    //private void Start()
    //{
    //    // 플레이어 캐릭터의 사망 이벤트 발생시 게임 오버
    //    FindObjectOfType<PlayerHealth>().onDeath += EndGame;
    //}

    //// 게임 오버 처리
    //public void EndGame()
    //{
    //    // 게임 오버 상태를 참으로 변경
    //    isGameover = true;
    //    // 게임 오버 UI를 활성화
    //    UIManager.instance.SetActiveGameoverUI(true);
    //}

    private void Start()
    {
        // 마우스를 가운데 고정된 상태에서 보이지 않게함
        Application.targetFrameRate = 60;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        if (isOpenInventory || isOpenCraftManual || isOpenComputer || isOpenArchemyTable || isPause)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            canPlayerMove = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            canPlayerMove = true;
        }
    }

    public void ActivateAllObjects()
    {
        //SoundManager.instance.StopNPauseBGM(false);
        SoundManager.instance.PlayBGM("TownBGM");
        foreach (var go in GameObjects)
        {
            if (go != null)
                go.SetActive(true);
        }
    }

    public void TransScene(string SceneName)
    {
        //SoundManager.instance.StopNPauseBGM(true);

        foreach (var go in GameObjects)
        {
            if(go != null)
                go.SetActive(false);
        }

        SoundManager.instance.PlayBGM("MiniGame");

        SceneManager.LoadSceneAsync(SceneName, LoadSceneMode.Additive);
    }
}
