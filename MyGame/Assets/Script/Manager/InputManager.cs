﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager instance
    {
        get
        {
            // 만약 싱글톤 변수에 아직 오브젝트가 할당되지 않았다면
            if (m_instance == null)
            {
                // 씬에서 GameManager 오브젝트를 찾아 할당
                m_instance = FindObjectOfType<InputManager>();
            }

            // 싱글톤 오브젝트를 반환
            return m_instance;
        }
    }

    private static InputManager m_instance;

    public float MouseX { get; private set; }           
    public float MouseY { get; private set; }       

    public float MoveX { get; private set; }
    public float MoveY { get; private set; }

    public bool SpaceKey { get; private set; }

    void Awake()
    {
        if(m_instance == null)
        {
            m_instance = this;
            DontDestroyOnLoad(gameObject);
        } 
        else
        {
            Destroy(gameObject);
        }

    }

    // Update is called once per frame
    void Update()
    {
        MouseX = Input.GetAxis("Mouse X");
        MouseY = Input.GetAxis("Mouse Y");
        MoveX = Input.GetAxisRaw("Horizontal");
        MoveY = Input.GetAxisRaw("Vertical");

        SpaceKey = Input.GetKeyDown(KeyCode.Space);
    }
}
