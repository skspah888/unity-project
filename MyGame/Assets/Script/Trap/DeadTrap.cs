﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadTrap : MonoBehaviour
{
    private Animator anim;
    private bool isActivated = false;

    [SerializeField] private string sound_Activate = "";
    [SerializeField] private TrapDamage theTrapDamage = null;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public bool GetIsActivated()
    {
        return isActivated;
    }

    public void ReInstall()
    {
        isActivated = false;
        anim.SetTrigger("DeActivate");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isActivated)
        {
            if (other.transform.tag != "Untagged" && other.transform.tag != "Trap")
            {
                StartCoroutine(theTrapDamage.ActivatedTrapCoroutine());
                isActivated = true;
                anim.SetTrigger("Activate");
                SoundManager.instance.PlaySE(sound_Activate);
                StartCoroutine(ResetTrapCoroutine());
            }
        }
    }

    private IEnumerator ResetTrapCoroutine()
    {
        yield return new WaitForSeconds(10);

        if (isActivated)
            ReInstall();
    }

}
