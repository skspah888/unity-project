﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapDamage : MonoBehaviour
{
    [SerializeField] private int damage = 0;
    [SerializeField] private float finishTime = 0f;
    private bool isHurt = false;
    private bool isActivated = false;



    public IEnumerator ActivatedTrapCoroutine()
    {
        isActivated = true;
        yield return new WaitForSeconds(finishTime);
        isActivated = false;
        isHurt = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isActivated)
        {
            if (!isHurt)
            {
                if (other.tag == "Player")
                {
                    isHurt = true;
                    other.transform.GetComponent<PlayerController>().OnDamage(damage, transform.position, transform.up);
                }
            }
        }
    }
}
