﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour , IDialogable
{
    public void NPCActivate()
    {
        GameManager.instance.TransScene("MiniGame");
        int iNumber = Random.Range(0, 4);
        if(iNumber == 0)
            SoundManager.instance.PlaySE("NPC_Sound1");
        else if (iNumber == 2)
            SoundManager.instance.PlaySE("NPC_Sound2");
    }
}
