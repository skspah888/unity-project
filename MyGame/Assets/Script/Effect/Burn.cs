﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Burn : MonoBehaviour
{
    private bool isBurning = false;

    [SerializeField] private int damage = 0;

    [SerializeField] private float damageTime = 0f;
    private float currentDamageTime;

    [SerializeField] private float durtationTime = 0f;
    private float currentDurationTime;

    [SerializeField] private GameObject flame_prefab = null; // 불 붙으면 프리팹 생성
    private GameObject go_tempFlame; // 그릇

    public void StartBurning()
    {
        // 동시 생성 방지
        if(!isBurning)
        {
            go_tempFlame = Instantiate(flame_prefab, transform.position + transform.up, Quaternion.Euler(new Vector3(-90f,0f,0f)));
            go_tempFlame.transform.SetParent(transform);
        }

        isBurning = true;
        currentDurationTime = durtationTime;
    }

    // Update is called once per frame
    void Update()
    {
        if(isBurning)
        {
            ElapseTime();
        }
    }

    private void ElapseTime()
    {
        if(isBurning)
        {
            currentDurationTime -= Time.deltaTime;

            if (currentDamageTime > 0)
                currentDamageTime -= Time.deltaTime;

            if(currentDamageTime <= 0)
            {
                // 데미지 입힘
                Damage();
            }

            if(currentDurationTime <= 0)
            {
                // 불을 끔
                Off();
            }

        }
    }

    private void Damage()
    {
        currentDamageTime = damageTime;
        UIManager.instance.DecreaseHP(damage);
        // GetComponent<PlayerController>().OnDamage(damage, transform.position, transform.forward);
    }

    private void Off()
    {
        isBurning = false;
        Destroy(go_tempFlame);
    }
}
