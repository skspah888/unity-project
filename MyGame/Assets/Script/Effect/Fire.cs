﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    [SerializeField] private string fireName = ""; // 불의 이름

    [SerializeField] private int damage = 0; // 불의 데미지

    [SerializeField] private float damageTime = 0f; // 데미지 딜레이
    private float currentDamageTime;

    [SerializeField] private float durationTime = 0f; // 불의 지속시간
    private float currentDurationTime;

    [SerializeField] private ParticleSystem ps_Flame = null; // 파티클 시스템

    // 상태 변수
    private bool isFire = true;

    // 필요한 컴포넌트
    private PlayerController playerController;

    private void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
        currentDurationTime = durationTime;
    }

    // Update is called once per frame
    void Update()
    {
        if(isFire)
        {
            ElapseTime();
        }
    }

    private void ElapseTime()
    {
        currentDurationTime -= Time.deltaTime;

        if (currentDamageTime >= 0)
            currentDamageTime -= Time.deltaTime;

        if(currentDurationTime <= 0)
        {
            // 불끔
            Off();
        }
    }

    private void Off()
    {
        ps_Flame.Stop();
        isFire = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if(isFire && other.transform.tag == "Player")
        {
            if(currentDamageTime <= 0)
            {
                // 상대방의 피격 위치와 피격 방향을 근삿값으로 계산
                Vector3 hitPoint = other.ClosestPoint(transform.position);
                Vector3 hitnormal = transform.position - other.transform.position;

                other.GetComponent<Burn>().StartBurning();
                playerController.OnDamage(damage, hitPoint, hitnormal);
                currentDamageTime = damageTime;
            }
          
        }
    }
}
