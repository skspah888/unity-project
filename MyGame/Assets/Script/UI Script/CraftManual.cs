﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Craft
{
    public string craftName; // 이름
    public Sprite craftImage; // 이미지
    public string craftDesc; // 설명
    public string[] craftNeedItem; // 필요 아이템
    public int[] craftNeedItemCount; // 필요 개수
    public GameObject go_Prefab; // 실제 설치될 프리팹
    public GameObject go_PreviewPrefab; // 미리보기 프리팹
}

public class CraftManual : MonoBehaviour
{
    // 상태 변수
    static public bool isActivated = false;
    static public bool isPreviewActivated = false;

    [SerializeField] private GameObject go_BaseUI = null;

    private int tabNumber = 0;
    private int page = 1;
    private int selectedSlotNumber;
    private Craft[] craft_SelectedTab;

    [SerializeField] private Craft[] craft_fire = null;         // 모닥불
    [SerializeField] private Craft[] craft_build = null;        // 건축

    private GameObject go_Preview; // 미리보기 프리팹을 담을 변수
    private GameObject go_Prefab;

    [SerializeField] private Transform tf_FPS = null;
    [SerializeField] private Transform tf_TPS = null;

    // RayCast 필요 변수 선언
    private RaycastHit hitInfo;
    [SerializeField]  private LayerMask layerMask = 0;
    [SerializeField] private float range = 0f;

    // 필요한 UI Slot 요소
    [SerializeField] private GameObject[] go_Slots = null;
    [SerializeField] private Image[] image_Slot = null;
    [SerializeField] private Text[] text_SlotName = null;
    [SerializeField] private Text[] text_SlotDesc = null;
    [SerializeField] private Text[] text_SlotNeedItem = null;

    // 필요 컴포넌트
    private Inventory theInventory;

    private void Start()
    {
        theInventory = FindObjectOfType<Inventory>();
        tabNumber = 0;
        page = 1;
        TabSlotSetting(craft_fire);
    }

    public void TabSetting(int _tabNumber)
    {
        tabNumber = _tabNumber;
        page = 1;

        switch(tabNumber)
        {
            case 0:     // 불 셋팅
                TabSlotSetting(craft_fire);
                break;

            case 1:     // 건축 셋팅
                TabSlotSetting(craft_build);
                break;
        }
    }

    private void ClearSlot()
    {
        for (int i = 0; i < go_Slots.Length; i++)
        {
            image_Slot[i].sprite = null;
            text_SlotDesc[i].text = "";
            text_SlotName[i].text = "";
            text_SlotNeedItem[i].text = "";
            go_Slots[i].SetActive(false);
        }
    }

    public void RightPageSetting()
    {
        if (page < (craft_SelectedTab.Length / go_Slots.Length) + 1)
            page++;
        else
            page = 1;

        TabSlotSetting(craft_SelectedTab);
    }

    public void LeftPageSetting()
    {
        if (page != 1)
            page--;
        else
            page = (craft_SelectedTab.Length / go_Slots.Length) + 1;

        TabSlotSetting(craft_SelectedTab);
    }

    private void TabSlotSetting(Craft[] _craft_tab)
    {
        ClearSlot();
        craft_SelectedTab = _craft_tab;
        int startSlotNumber = (page - 1) * go_Slots.Length;     // 0 4 8 ...

        for (int i = startSlotNumber; i < craft_SelectedTab.Length; i++)
        {
            if (i == page * go_Slots.Length)
                break;

            go_Slots[i - startSlotNumber].SetActive(true);

            image_Slot[i - startSlotNumber].sprite = craft_SelectedTab[i].craftImage;
            text_SlotName[i - startSlotNumber].text = craft_SelectedTab[i].craftName;
            text_SlotDesc[i - startSlotNumber].text = craft_SelectedTab[i].craftDesc;
            for(int j=0; j < craft_SelectedTab[i].craftNeedItem.Length; j++)
            {
                text_SlotNeedItem[i - startSlotNumber].text += craft_SelectedTab[i].craftNeedItem[j];
                text_SlotNeedItem[i - startSlotNumber].text += " x " + craft_SelectedTab[i].craftNeedItemCount[j] + "\n";
            }
            
        }
    }

    public void SlotClick(int _slotNumber)
    {
        selectedSlotNumber = _slotNumber + (page - 1) * go_Slots.Length;

        if (!CheckIngredient())
            return;

        if (CameraManager.instance.IsTPS)
        {
            go_Preview = Instantiate(craft_SelectedTab[selectedSlotNumber].go_PreviewPrefab, tf_TPS.position + tf_TPS.forward * 2f, Quaternion.identity);
        }
        else // 1인칭일때 -> CrossHair?
        {
            go_Preview = Instantiate(craft_SelectedTab[selectedSlotNumber].go_PreviewPrefab, tf_FPS.position + tf_FPS.forward * 2f, Quaternion.identity);
        }

        go_Prefab = craft_SelectedTab[selectedSlotNumber].go_Prefab;
        isActivated = false;
        isPreviewActivated = true;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        go_BaseUI.SetActive(false);
    }

    private bool CheckIngredient()
    {
        for (int i = 0; i < craft_SelectedTab[selectedSlotNumber].craftNeedItem.Length; i++)
        {
            if (theInventory.GetItemCount(craft_SelectedTab[selectedSlotNumber].craftNeedItem[i]) < craft_SelectedTab[selectedSlotNumber].craftNeedItemCount[i])
                return false;

        }

        return true;
    }

    private void UseIngredient()
    {
        for (int i = 0; i < craft_SelectedTab[selectedSlotNumber].craftNeedItem.Length; i++)
        {
            theInventory.SetItemCount(craft_SelectedTab[selectedSlotNumber].craftNeedItem[i], craft_SelectedTab[selectedSlotNumber].craftNeedItemCount[i]);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab) && !isPreviewActivated)
            Window();

        if (isPreviewActivated)
            PreviewPositionUpdate();

        if (Input.GetButtonDown("Fire1"))
            Build();

        if (Input.GetKeyDown(KeyCode.Escape))
            Cancel();

        GameManager.isOpenCraftManual = isActivated;
    }

    private void Build()
    {
        if(isPreviewActivated && go_Preview.GetComponent<PreviewObject>().isBuildable())
        {
            UseIngredient();
            Instantiate(go_Prefab, go_Preview.transform.position, go_Preview.transform.rotation);
            Destroy(go_Preview);
            isActivated = false;
            isPreviewActivated = false;
            go_Preview = null;
            go_Prefab = null;
        }
    }

    private void PreviewPositionUpdate()
    {
        // 3인칭일때 -> CameraArm따라가게함?
        if (CameraManager.instance.IsTPS)
        {
            if (Physics.Raycast(tf_TPS.position + tf_TPS.up, tf_TPS.forward  + (-tf_TPS.up)*0.3f, out hitInfo, range, layerMask))
            {
                if (hitInfo.transform != null)
                {
                    Vector3 _location = hitInfo.point;

                    if (Input.GetKeyDown(KeyCode.Q))
                        go_Preview.transform.Rotate(0, -90f, 0f);
                    else if (Input.GetKeyDown(KeyCode.E))
                        go_Preview.transform.Rotate(0,  90f, 0f);

                    _location.Set(Mathf.Round(_location.x), Mathf.Round(_location.y / 0.1f) * 0.1f, Mathf.Round(_location.z));
                    go_Preview.transform.position = _location;
                }
            }
        }
        else // 1인칭일때 -> CrossHair?
        {
            if (Physics.Raycast(tf_FPS.position, tf_FPS.forward, out hitInfo, range, layerMask))
            {
                if (hitInfo.transform != null)
                {
                    Vector3 _location = hitInfo.point;

                    if (Input.GetKeyDown(KeyCode.Q))
                        go_Preview.transform.Rotate(0, -90f, 0f);
                    else if (Input.GetKeyDown(KeyCode.E))
                        go_Preview.transform.Rotate(0, 90f, 0f);

                    _location.Set(Mathf.Round(_location.x), Mathf.Round(_location.y / 0.1f) * 0.1f, Mathf.Round(_location.z));
                    go_Preview.transform.position = _location;
                }
            }
        }
           
    }

    private void Cancel()
    {
        if (isPreviewActivated)
            Destroy(go_Preview);

        isActivated = false;
        isPreviewActivated = false;
        go_Preview = null;
        go_Prefab = null;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        go_BaseUI.SetActive(false);
    }

    private void Window()
    {
        if (!isActivated)
            OpenWindow();
        else
            CloseWindow();
    }


    private void OpenWindow()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        isActivated = true;
        go_BaseUI.SetActive(true);
    }

    private void CloseWindow()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        isActivated = false;
        go_BaseUI.SetActive(false);
    }
}
