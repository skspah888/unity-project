﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour
{
    // 크로스 헤어 애니메이션
    [SerializeField] private Animator animator = null;

    // 크로스 헤어 비활성화를 위한 부모 객체
    [SerializeField] private GameObject go_CrosshairHUD = null;

    public void WalkingAnimation(bool _flag)
    {
        if(CameraManager.instance.IsTPS)
            return;

        animator.SetBool("Walk", _flag);
    }

    public void RunningAnimation(bool _flag)
    {
        if (CameraManager.instance.IsTPS)
            return;

        animator.SetBool("Run", _flag);
    }

    public void JumpAnimation(bool _flag)
    {
        if (CameraManager.instance.IsTPS)
            return;

        animator.SetBool("Run", _flag);
    }

    public void CrouchingAnimation(bool _flag)
    {
        if (CameraManager.instance.IsTPS)
            return;

        animator.SetBool("Crouch", _flag);
    }

    public void FineSightAnimation(bool _flag)
    {
        if (CameraManager.instance.IsTPS)
            return;

        animator.SetBool("FineSight", _flag);
    }

    public void AttackAnimation()
    {
        if (CameraManager.instance.IsTPS)
            return;

        if (animator.GetBool("Walk") || animator.GetBool("Run"))
            animator.SetTrigger("Walk_Fire");
        else if (animator.GetBool("Crouch"))
            animator.SetTrigger("Crouch_Fire");
        else
            animator.SetTrigger("Idle_Fire");
    }

    public void SetGameObject(bool isValue)
    {
        go_CrosshairHUD.SetActive(isValue);
    }
}
