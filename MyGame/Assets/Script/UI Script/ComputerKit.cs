﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Kit
{
    public string kitName;
    public string kitDescription;
    public string[] needItemName;
    public int[] needItemNumber;

    public GameObject go_Kit_Prefab;
}

public class ComputerKit : MonoBehaviour
{
    [SerializeField] private Kit[] kits = null;
    [SerializeField] private Transform tf_ItemAppear = null;     // 생성될 아이템 위치
    [SerializeField] private GameObject go_BaseUI = null;     

    private bool isCraft = false;       // 중복 실행 방지
    public bool isPowerOn = false;      // 전원 확인

    [SerializeField] private string sound_ButtonClick = "";
    [SerializeField] private string sound_Beep = "";
    [SerializeField] private string sound_Activated = "";
    [SerializeField] private string sound_Output = "";

    // 필요 컴포넌트
    private Inventory theInven;

    [SerializeField] private ComputerToolTip theToolTip = null;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        theInven = FindObjectOfType<Inventory>();
    }

    private void Update()
    {
        if (isPowerOn)
            if (Input.GetKeyDown(KeyCode.Escape))
                PowerOff();
    }

    public void PowerOn()
    {
        GameManager.isOpenComputer = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        isPowerOn = true;
        go_BaseUI.SetActive(true);
    }

    private void PowerOff()
    {
        GameManager.isOpenComputer = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        isPowerOn = false;
        theToolTip.HideToolTip();
        go_BaseUI.SetActive(false);
    }

    public void ShowToolTip(int _buttonNum)
    {
        theToolTip.ShowToolTop(kits[_buttonNum].kitName, kits[_buttonNum].kitDescription, kits[_buttonNum].needItemName, kits[_buttonNum].needItemNumber);
    }

    public void HideToolTip()
    {
        theToolTip.HideToolTip();
    }

    public void ClickButton(int _slotNumber)
    {
        SoundManager.instance.PlaySE(sound_ButtonClick);

        if (!isCraft)
        {
            if (!CheckIngredient(_slotNumber))              // 재료 체크
                return;

            isCraft = true;
            UseIngredient(_slotNumber);                     // 재료 사용
            StartCoroutine(CraftCoroutine(_slotNumber));    // 생성
        }
    }

    private bool CheckIngredient(int _slotNumber)
    {
        for (int i = 0; i < kits[_slotNumber].needItemName.Length; i++)
        {
            if (kits[_slotNumber] != null)
            {
                if (theInven.GetItemCount(kits[_slotNumber].needItemName[i]) < kits[_slotNumber].needItemNumber[i])
                {
                    SoundManager.instance.PlaySE(sound_Beep);
                    return false;
                }
            }
        }
        return true;
    }

    private void UseIngredient(int _slotNumber)
    {
        for (int i = 0; i < kits[_slotNumber].needItemName.Length; i++)
            theInven.SetItemCount(kits[_slotNumber].needItemName[i], kits[_slotNumber].needItemNumber[i]);
    }

    IEnumerator CraftCoroutine(int _slotNumber)
    {
        SoundManager.instance.PlaySE(sound_Activated);
        yield return new WaitForSeconds(3f);

        SoundManager.instance.PlaySE(sound_Output);

        Instantiate(kits[_slotNumber].go_Kit_Prefab, tf_ItemAppear.position, Quaternion.identity);

        isCraft = false;

    }

}
