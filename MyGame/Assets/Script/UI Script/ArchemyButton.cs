﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class ArchemyButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private ArchemyTable theArchemy = null;
    [SerializeField] private int buttonNum = 0;


    public void OnPointerEnter(PointerEventData eventData)
    {
        theArchemy.ShowToolTip(buttonNum);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        theArchemy.HideToolTip();
    }
}
