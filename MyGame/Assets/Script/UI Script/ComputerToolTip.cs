﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComputerToolTip : MonoBehaviour
{
    [SerializeField] private GameObject go_BaseUi = null;
    [SerializeField] private Text kitName = null;
    [SerializeField] private Text kitDesc = null;
    [SerializeField] private Text kitNeedItem = null;


    public void ShowToolTop(string _kitName, string _kitDesc, string[] _needItem, int[] _needItemNumber)
    {
        go_BaseUi.SetActive(true);
        kitName.text = _kitName;
        kitDesc.text = _kitDesc;

        for (int i = 0; i < _needItem.Length; i++)
        {
            kitNeedItem.text += _needItem[i];
            kitNeedItem.text += " x " + _needItemNumber[i].ToString() + "\n";
        }
    }

    public void HideToolTip()
    {
        kitName.text = "";
        kitDesc.text = "";
        kitNeedItem.text = "";
        go_BaseUi.SetActive(false);
    }
}
