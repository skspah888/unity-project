﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputNumber : MonoBehaviour
{
    private bool activate = false;
    [SerializeField] private Text text_PreView = null;
    [SerializeField] private Text text_Input = null;
    [SerializeField] private InputField if_Text = null;

    [SerializeField] private GameObject go_Base = null;
    [SerializeField] private ActionController thePlayer = null;

    private void Update()
    {
        if (activate)
        {
            if (Input.GetKeyDown(KeyCode.Return))
                OK();
            else if (Input.GetKeyDown(KeyCode.Escape))
                Cancel();
        }
    }

    public void Call()
    {
        go_Base.SetActive(true);
        activate = true;
        if_Text.text = "";
        text_PreView.text = DragSlot.instance.dragSlot.itemCount.ToString();
    }

    public void Cancel()
    {
        go_Base.SetActive(false);
        activate = false;
        DragSlot.instance.SetColor(0);
        DragSlot.instance.dragSlot = null;
    }

    public void OK()
    {
        DragSlot.instance.SetColor(0);
        int num = 0;
        if(text_Input.text != "")
        {
            if(CheckNumber(text_Input.text))
            {
                num = int.Parse(text_Input.text);

                // 최대 판별
                if (num > DragSlot.instance.dragSlot.itemCount)
                    num = DragSlot.instance.dragSlot.itemCount;
            }
            else
            {
                num = 1;
            }
        }
        else
        {
            // 아무거도 안적으면 preview
            num = int.Parse(text_PreView.text);
        }

        StartCoroutine(DropItemCoroutine(num));
    }

    IEnumerator DropItemCoroutine(int num)
    {
        for (int i = 0; i < num; i++)
        {
            if(DragSlot.instance.dragSlot.item.itemPrefab != null)
                Instantiate(DragSlot.instance.dragSlot.item.itemPrefab,
                thePlayer.transform.position + (thePlayer.transform.forward * 1.3f) + (thePlayer.transform.up * 1.7f), Quaternion.identity);

            DragSlot.instance.dragSlot.SetSlotCount(-1);
            yield return new WaitForSeconds(0.1f);
        }

        DragSlot.instance.dragSlot = null;
        activate = false;
        go_Base.SetActive(false);
    }

    private bool CheckNumber(string text)
    {
        // 문자열을 캐릭터배열로 변환 => 아스키코드로 숫자/문자 판별
        char[] tempCharArray = text.ToCharArray();
        bool isNumber = true;
        for (int i = 0; i < tempCharArray.Length; i++)
        {
            if (tempCharArray[i] >= 48 && tempCharArray[i] <= 57)
                continue;
            isNumber = false;
        }

        return isNumber;

    }
}
