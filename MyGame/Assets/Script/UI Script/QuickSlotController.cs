﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuickSlotController : MonoBehaviour
{
    [SerializeField] private Slot[] quickSlots = null;          // 퀵슬롯들.
    [SerializeField] private Image[] img_CoolTime = null;       // 퀵슬롯 쿨타임
    [SerializeField] private Transform tf_parent = null;

    private int selectedSlot;       // 선택된 퀵슬롯 
    private int currentSlot;       // 선택된 퀵슬롯 

    // 선택된 퀵슬롯의 이미지
    [SerializeField] private GameObject go_SelectedImage = null;
    [SerializeField] private float CoolTime = 0f;
    private float currentCoolTime = 0f;
    private bool isCoolTime;
    private ItemEffectDatabase theItemEffectDatabase;

    private Animator anim;
    private PlayerController theplayerController;

    [SerializeField] private float appearTime = 0f;
    private float currentAppearTime = 0f;
    private bool isAppear;

    // Start is called before the first frame update-
    void Start()
    {
        anim = GetComponent<Animator>();
        theplayerController = FindObjectOfType<PlayerController>();
        theItemEffectDatabase = FindObjectOfType<ItemEffectDatabase>();
        quickSlots = tf_parent.GetComponentsInChildren<Slot>();
        selectedSlot = 0;
        currentSlot = 0;
    }

    // Update is called once per frame
    void Update()
    {
        TryInputNumber();
        CoolTimeCalc();
        AppearCalc();
        
    }

    public void AppearReset()
    {
        currentAppearTime = appearTime;
        isAppear = true;
        anim.SetBool("Appear", isAppear);
    }

    private void AppearCalc()
    {
        if (Inventory.inventoryActivated)
            AppearReset();
        else
        {
            if (isAppear)
            {
                currentAppearTime -= Time.deltaTime;
                if (currentAppearTime <= 0)
                {
                    isAppear = false;
                    anim.SetBool("Appear", isAppear);
                }
            }
        }
    }

    private void CoolTimeReset()
    {
        currentCoolTime = CoolTime;
        isCoolTime = true;
    }

    private void CoolTimeCalc()
    {
        if(isCoolTime)
        {
            currentCoolTime -= Time.deltaTime;

            for (int i = 0; i < img_CoolTime.Length; i++)
                img_CoolTime[i].fillAmount = currentCoolTime / CoolTime;
            
            if (currentCoolTime <= 0)
                isCoolTime = false;
        }
    }

    private void TryInputNumber()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            ChangeSlot(0);
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            ChangeSlot(1);
        else if (Input.GetKeyDown(KeyCode.Alpha3))
            ChangeSlot(2);
        else if (Input.GetKeyDown(KeyCode.Alpha4))
            ChangeSlot(3);
        else if (Input.GetKeyDown(KeyCode.Alpha5))
            ChangeSlot(4);
        else if (Input.GetKeyDown(KeyCode.Alpha6))
            ChangeSlot(5);
        else if (Input.GetKeyDown(KeyCode.Alpha7))
            ChangeSlot(6);
        else if (Input.GetKeyDown(KeyCode.Alpha8))
            ChangeSlot(7);

        if (Input.GetKeyDown(KeyCode.LeftAlt))
        {
            if (isAppear)
            {
                isAppear = false;
                anim.SetBool("Appear", isAppear);
            }
            else
            {
                AppearReset();
            }
        }
    }

    private void ChangeSlot(int num)
    {
        SelectedSlot(num);
        AppearReset();

        if (selectedSlot == currentSlot && !isCoolTime)
            Execute();
        else
        {
            CheckKitItem(currentSlot, selectedSlot);
            currentSlot = selectedSlot;
        }
    }

    private void CheckKitItem(int _currentSlot, int _selectedSlot)
    {
        if (quickSlots[_selectedSlot].item != null)
        {
            if (quickSlots[_selectedSlot].item.itemType == Item.ItemType.Kit)
            {
                // 아이템 프리뷰
                PlayerController.currentKit = quickSlots[_selectedSlot].item;
            }
        }

        if (quickSlots[_currentSlot].item != null)
        {
            if (quickSlots[_currentSlot].item.itemType == Item.ItemType.Kit)
            {
                theplayerController.Cancel();
            }
        }
    }

    private void SelectedSlot(int num)
    {
        // 선택된 슬롯
        selectedSlot = num;

        // 선택된 슬롯으로 이미지 이동
        go_SelectedImage.transform.position = quickSlots[selectedSlot].transform.position;

        if (quickSlots[selectedSlot].item != null)
        {
            if (quickSlots[selectedSlot].item.itemType == Item.ItemType.Kit)
            {
                // 아이템 프리뷰
                PlayerController.currentKit = quickSlots[selectedSlot].item;
            }
        }
    }



    private void Execute()
    {
        if (quickSlots[selectedSlot].item != null)
        {
            if(quickSlots[selectedSlot].item.itemType == Item.ItemType.Equipment)
            {
                ;// 장비 변경 [ 미구현 ]
            }
            else if (quickSlots[selectedSlot].item.itemType == Item.ItemType.Used)
            {
                // 아이템 사용
                theItemEffectDatabase.UseItem(quickSlots[selectedSlot].item);
                quickSlots[selectedSlot].SetSlotCount(-1);
            }
            CoolTimeReset();
        }
        else
        {

        }
    }

    public void DecreaseSelectedItem()
    {
        if(quickSlots[selectedSlot] != null)
        {
            if(quickSlots[selectedSlot].item != null)
            {
                quickSlots[selectedSlot].SetSlotCount(-1);
            }
        }
    }
}
