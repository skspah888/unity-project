﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlotToolTip : MonoBehaviour
{
    // 필요 컴포넌트
    [SerializeField] private GameObject go_Base = null;
    [SerializeField] private Text text_ItemName = null;
    [SerializeField] private Text text_ItemDesc = null;
    [SerializeField] private Text text_ItemHowtoUsed = null;

    public void ShowToolTip(Item item, Vector3 pos)
    {
        go_Base.SetActive(true);
        pos += new Vector3(go_Base.GetComponent<RectTransform>().rect.width * 0.5f, -go_Base.GetComponent<RectTransform>().rect.height, 0f);
        go_Base.transform.position = pos;


        text_ItemName.text = item.itemName;
        text_ItemDesc.text = item.itemDesc;

        if (item.itemType == Item.ItemType.Equipment)
            text_ItemHowtoUsed.text = "우클릭 - 장착";
        else if (item.itemType == Item.ItemType.Used)
            text_ItemHowtoUsed.text = "우클릭 - 사용";
        else
            text_ItemHowtoUsed.text = "";
    }

    public void HideToolTip()
    {
        go_Base.SetActive(false);
    }

}
