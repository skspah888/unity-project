﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusController : MonoBehaviour
{
    // 체력
    [SerializeField] private int Maxhp = 0;
    private int currentHp = 0;

    // 스태미나
    [SerializeField] private int Maxsp = 0;
    private int currentSp = 0;

    // 스태미나 증가량
    [SerializeField] private int spIncreaseSpeed = 0;
    // 스태미나 재회복 딜레이
    [SerializeField] private int spRechargeTime = 0;
    private int currentSpRechargeTime = 0;

    // 스태미나 감소 여부
    private bool spUsed = false;

    // 마나
    [SerializeField] private int Maxmp = 0;
    private int currentMp = 0;
    
    // 배고픔
    [SerializeField] private int Maxhungry = 0;
    private int currentHungry = 0;

    // 배고픔이 줄어드는 속도
    [SerializeField] private int hungryDecreaseTime = 0;
    private int currentHungryDecreaseTime = 0;

    // 목마름
    [SerializeField] private int Maxthirsty = 0;
    private int currentThirsty = 0;

    // 목마름이 줄어드는 속도
    [SerializeField] private int thirstyDecreaseTime = 0;
    private int currentThirstyDecreaseTime = 0;


    [SerializeField] private int Maxsatisfy = 0;
    private int currentSatisfy = 0;

    [SerializeField] private Image[] images_Gauge = null;

    private const int HP = 0, MP = 1, SP = 2, HUNGRY = 3, THIRSTY = 4, SATISFY = 5;

    // Start is called before the first frame update
    void Start()
    {
        currentHp = Maxhp;
        currentSp = Maxsp;
        currentMp = Maxmp;
        currentHungry = Maxhungry;
        currentThirsty = Maxthirsty;
        currentSatisfy = Maxsatisfy;
    }

    // Update is called once per frame
    void Update()
    {
        Hungry();
        Thirsty();
        SPRechargeTime();
        SPRecover();
        GaugeUpdate();
    }

    private void SPRechargeTime()
    {
        if(spUsed)
        {
            if (currentSpRechargeTime < spRechargeTime)
                currentSpRechargeTime++;
            else
                spUsed = false;   
        }
    }

    private void SPRecover()
    {
        if(!spUsed && currentSp < Maxsp)
        {
            currentSp += spIncreaseSpeed;
        }
    }

    private void Hungry()
    {
        // 감소 시간이 되면 Hungry 감소
        if(currentHungry > 0)
        {
            if (currentHungryDecreaseTime <= hungryDecreaseTime)
                currentHungryDecreaseTime++;
            else
            {
                currentHungry--;
                currentHungryDecreaseTime = 0;
            }
        }
        else
        {
            Debug.Log("배고픔 수치가 0이 되었습니다.");
        }
    }

    private void Thirsty()
    {
        // 감소 시간이 되면 Thirsty 감소 [ 감소 수치는 다르게 주기 ]
        if (currentThirsty > 0)
        {
            if (currentThirstyDecreaseTime <= thirstyDecreaseTime)
                currentThirstyDecreaseTime++;
            else
            {
                currentThirsty--;
                currentThirstyDecreaseTime = 0;
            }
        }
        else
        {
            Debug.Log("목마름 수치가 0이 되었습니다.");
        }
    }

    private void GaugeUpdate()
    {
        images_Gauge[HP].fillAmount = (float)currentHp / Maxhp;
        images_Gauge[SP].fillAmount = (float)currentSp / Maxsp;
        images_Gauge[MP].fillAmount = (float)currentMp / Maxmp;

        images_Gauge[HUNGRY].fillAmount = (float)currentHungry / Maxhungry;
        images_Gauge[THIRSTY].fillAmount = (float)currentThirsty / Maxthirsty;
        images_Gauge[SATISFY].fillAmount = (float)currentSatisfy / Maxsatisfy;
    }

    public void IncreaseHP(int _count)
    {
        if (currentHp + _count < Maxhp)
            currentHp += _count;
        else
            currentHp = Maxhp;
    }

    public void DecreaseHP(int _count)
    {
        currentHp -= _count;
        if (currentHp <= 0)
            Debug.Log("캐릭터 hp가 0이 되었습니다.");
    }

    public void IncreaseSP(int _count)
    {
        if (currentSp + _count < Maxsp)
            currentSp += _count;
        else
            currentSp = Maxsp;
    }

    public void DecreaseSP(int _count)
    {
        currentSp -= _count;
        if (currentSp <= 0)
        {
            currentSp = 0;
            Debug.Log("캐릭터 Sp가 0이 되었습니다.");
        }
    }

    public void IncreaseMP(int _count)
    {
        if (currentMp + _count < Maxmp)
            currentMp += _count;
        else
            currentMp = Maxmp;
    }

    public void DecreaseMP(int _count)
    {
        currentMp -= _count;
        if (currentMp <= 0)
        {
            currentMp = 0;
            Debug.Log("캐릭터 mp가 0이 되었습니다.");
        }
    }

    public void IncreaseHungry(int _count)
    {
        if (currentHungry + _count < Maxhungry)
            currentHungry += _count;
        else
            currentHungry = Maxhungry;
    }

    public void DecreaseHungry(int _count)
    {
        if (currentHungry - _count < 0)
            currentHungry = 0;
        else
            currentHungry -= _count;
    }

    public void IncreaseThirsty(int _count)
    {
        if (currentThirsty + _count < Maxthirsty)
            currentThirsty += _count;
        else
            currentThirsty = Maxthirsty;
    }

    public void DecreaseThirsty(int _count)
    {
        if (currentThirsty - _count < 0)
            currentThirsty = 0;
        else
            currentThirsty -= _count;
    }

    public void IncreaseSatisfy(int _count)
    {
        if (currentSatisfy + _count < Maxsatisfy)
            currentSatisfy += _count;
        else
            currentSatisfy = Maxsatisfy;
    }

    public void DecreaseSatisfy(int _count)
    {
        if (currentSatisfy - _count < 0)
            currentSatisfy = 0;
        else
            currentSatisfy -= _count;
    }

    public void DecreaseStamina(int _count)
    {
        spUsed = true;
        currentSpRechargeTime = 0;

        if (currentSp - _count > 0)
            currentSp -= _count;
        else
            currentSp = 0;
    }

    public int GetCurrentSP() { return currentSp; }
    public int GetCurrentHP() { return currentHp; }
    public int GetCurrentMP() { return currentMp; }
}
