﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ComputerButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private ComputerKit theComputer = null;
    [SerializeField] private int buttonNum = 0;

    public void OnPointerEnter(PointerEventData eventData)
    {
        theComputer.ShowToolTip(buttonNum);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        theComputer.HideToolTip();
    }
}
