﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    // 마우스 움직일때 카메라 회전 막기, 공격 기능 막기, 활성화 용도
    public static bool inventoryActivated = false;

    // 필요 컴포넌트
    [SerializeField] private GameObject go_InventoryBase = null;
    [SerializeField] private GameObject go_SlotsParent = null;

    private ItemEffectDatabase itemEffectDatabase;

    [SerializeField] private GameObject go_QuickSlotParent = null;

    // 인벤토리 슬롯들
    private Slot[] slots;
    private Slot[] quickslots;

    private bool isNotPut;
    private QuickSlotController thequickSlotController;

    public Slot[] GetSlots() { return slots; }

    [SerializeField] private Item[] items = null;

    public void LoadToInven(int _arrayNum, string _itemName, int _itemNum)
    {
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i].itemName == _itemName)
                slots[_arrayNum].AddItem(items[i], _itemNum);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        itemEffectDatabase = FindObjectOfType<ItemEffectDatabase>();
        slots = go_SlotsParent.GetComponentsInChildren<Slot>();
        quickslots = go_QuickSlotParent.GetComponentsInChildren<Slot>();
        thequickSlotController = FindObjectOfType<QuickSlotController>();
    }

    // Update is called once per frame
    void Update()
    {
        TryOpenInventory();
    }

    private void TryOpenInventory()
    {
        if(Input.GetKeyDown(KeyCode.I))
        {
            inventoryActivated = !inventoryActivated;

            if(inventoryActivated)
                OpenInventory();
            else
                CloseInventory();

        }
    }

    private void OpenInventory()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        GameManager.isOpenInventory = true;
        go_InventoryBase.SetActive(true);
    }

    private void CloseInventory()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        GameManager.isOpenInventory = false;
        itemEffectDatabase.HideToolTip();
        go_InventoryBase.SetActive(false);
    }

    public void AcquireItem(Item item, int count = 1)
    {
        //// 퀵슬롯부터 채우기
        //PutSlot(quickslots, item, count);

        //if (isNotPut)                       // 퀵슬롯이 꽉찼으면 인벤토리에 넣기
        //    PutSlot(slots, item, count);

        //if (isNotPut)                       // 둘다 꽉참
        //    Debug.Log("퀵슬롯과 인벤토리가 꽉찼습니다.");


        // 인벤토리부터 채우기
        PutSlot(slots, item, count);

        if (isNotPut)                       // 인벤토리가 꽉찼으면 퀵슬롯에 넣기
            PutSlot(quickslots, item, count);

        if (isNotPut)                       // 둘다 꽉참
            Debug.Log("퀵슬롯과 인벤토리가 꽉찼습니다.");

        thequickSlotController.AppearReset();
    }

    private void PutSlot(Slot[] _slots, Item _item, int _count)
    {
        if (Item.ItemType.Equipment != _item.itemType && Item.ItemType.Kit != _item.itemType)
        {
            for (int i = 0; i < _slots.Length; i++)
            {
                if (_slots[i].item != null)
                {
                    if (_slots[i].item.itemName == _item.itemName)
                    {
                        _slots[i].SetSlotCount(_count);
                        isNotPut = false;
                        return;
                    }
                }
            }
        }

        for (int i = 0; i < _slots.Length; i++)
        {
            if (_slots[i].item == null)
            {
                _slots[i].AddItem(_item, _count);
                isNotPut = false;
                return;
            }
        }

        isNotPut = true;
    }

    public int GetItemCount(string _itemName)
    {
        int temp = SearchSlotItem(slots, _itemName);

        return temp != 0 ? temp : SearchSlotItem(quickslots, _itemName);
    }

    private int SearchSlotItem(Slot[] _slots, string _itemName)
    {
        for (int i = 0; i < _slots.Length; i++)
        {
            if(_slots[i].item != null)
            {
                if (_itemName == _slots[i].item.itemName)
                    return _slots[i].itemCount;
            }
        }

        return 0;
    }

    public void SetItemCount(string _itemName, int _itemCount)
    {
        if (!ItemCountAdjust(slots, _itemName, _itemCount))
            ItemCountAdjust(quickslots, _itemName, _itemCount);
    }

    private bool ItemCountAdjust(Slot[] _slots, string _itemName, int _itemCount)
    {
        for (int i = 0; i < _slots.Length; i++)
        {
            if (_slots[i].item != null)
            {
                if (_itemName == _slots[i].item.itemName)
                {
                    _slots[i].SetSlotCount(-_itemCount);
                    return true;
                }
            }
        }
        return false;
    }

}
