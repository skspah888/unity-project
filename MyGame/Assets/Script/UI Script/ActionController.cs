﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionController : MonoBehaviour
{
    [SerializeField] private float range = 0f; // 습득 가능한 최대 거리.
    private bool pickupActivate = false; // 습득이 가능할 시 활성화

    private bool lookComputer = false;      // 컴퓨터를 바라볼 시 true;
    private bool lookArchemyTable = false;  // 연금 테이블 바라볼 시 true;
    private bool lookActivatedTrap = false; // 함정 바라볼 시 true;
    private bool lookNpc = false; // 함정 바라볼 시 true;

    private RaycastHit hitInfo;     // 충돌체 정보 저장

    // 아이템 레이어에만 반응하도록 레이어 마스크 설정
    [SerializeField] private LayerMask layerMask = 0;

    // 필요한 컴포넌트
    [SerializeField] private Text actionText = null;
    [SerializeField] private Inventory theInventory = null;

    [SerializeField] private Transform TPS_Camera= null;
    [SerializeField] private Transform FPS_Camera = null;

    private void Update()
    {
        CheckItem();
        TryAction();
    }

    private void TryAction()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            CheckItem();
            CanPickUp();
            CanComputerPowerOn();
            CanArchemyTableOpen();
            CanReInstallTrap();
            CanNpcDialogue();
        }
    }

    private void CanPickUp()
    {
        if (pickupActivate)
        {
            if (hitInfo.transform != null)
            {
                Debug.Log(hitInfo.transform.GetComponent<ItemPickUp>().item.itemName + " 획득했습니다.");
                theInventory.AcquireItem(hitInfo.transform.GetComponent<ItemPickUp>().item);
                Destroy(hitInfo.transform.gameObject);
                ItemInfoDisappear();
                SoundManager.instance.PlaySE("ItemGet");
            }
        }
    }

    private void CanComputerPowerOn()
    {
        if (lookComputer)
        {
            if (hitInfo.transform != null)
            {
                if(!hitInfo.transform.GetComponent<ComputerKit>().isPowerOn)
                {
                    hitInfo.transform.GetComponent<ComputerKit>().PowerOn();
                    ItemInfoDisappear();
                    SoundManager.instance.PlaySE("ItemGet");
                }
            }
        }
    }

    private void CanArchemyTableOpen()
    {
        if (lookArchemyTable)
        {
            if (hitInfo.transform != null)
            {
                hitInfo.transform.GetComponent<ArchemyTable>().Window();
                ItemInfoDisappear();
                SoundManager.instance.PlaySE("ItemGet");
            }
        }
    }

    private void CanReInstallTrap()
    {
        if (lookActivatedTrap)
        {
            if (hitInfo.transform != null)
            {
                hitInfo.transform.GetComponent<DeadTrap>().ReInstall();
                ItemInfoDisappear();
            }
        }
    }

    private void CanNpcDialogue()
    {
        if (lookNpc)
        {
            if (hitInfo.transform != null)
            {
                hitInfo.transform.GetComponent<NPC>().NPCActivate();
                ItemInfoDisappear();
                SoundManager.instance.PlaySE("ItemGet");
            }
        }
    }

    private void CheckItem()
    {
        // 3인칭 시점
        if (CameraManager.instance.IsTPS)
        {
            if (Physics.SphereCast(transform.position + transform.up, transform.lossyScale.y / 2, transform.forward + (-transform.up)*0.3f, out hitInfo, 0.5f))
            {
                if (hitInfo.transform.tag == "Item")
                    ItemInfoAppear();
                else if (hitInfo.transform.tag == "ArchemyTable")
                    ArchemyInfoAppear();
                else if (hitInfo.transform.tag == "Trap")
                    TrapInfoAppear();
                else if (hitInfo.transform.tag == "Npc")
                    NpcInfoAppear();
                //else if (hitInfo.transform.tag == "Computer")
                //    ComputerInfoAppear();
                else
                    ItemInfoDisappear();
            }
            else
                ItemInfoDisappear();
        }

        // 1인칭 시점
        else
        {
            if (Physics.Raycast(FPS_Camera.position, FPS_Camera.forward, out hitInfo, range, layerMask))
            {
                if (hitInfo.transform.tag == "Item")
                    ItemInfoAppear();
                else if (hitInfo.transform.tag == "Computer")
                    ComputerInfoAppear();
                else if (hitInfo.transform.tag == "ArchemyTable")
                    ArchemyInfoAppear();
                else if (hitInfo.transform.tag == "Trap")
                    TrapInfoAppear();
                else if (hitInfo.transform.tag == "Npc")
                    NpcInfoAppear();
                else
                    ItemInfoDisappear();
            }
            else
                ItemInfoDisappear();
        }
    }

    private void ItemInfoAppear()
    {
        Reset();
        pickupActivate = true;
        actionText.gameObject.SetActive(true);
        actionText.text = hitInfo.transform.GetComponent<ItemPickUp>().item.itemName + " 획득 " + "<color=yellow>" + "(E)" + "</color>";
    }

    private void ComputerInfoAppear()
    {
        if (!hitInfo.transform.GetComponent<ComputerKit>().isPowerOn)
        {
            Reset();
            lookComputer = true;
            actionText.gameObject.SetActive(true);
            actionText.text = " 컴퓨터 가동 " + "<color=yellow>" + "(E)" + "</color>";
        } 
    }

    private void ArchemyInfoAppear()
    {
        if (!hitInfo.transform.GetComponent<ArchemyTable>().GetIsOpen())
        {
            Reset();
            lookArchemyTable = true;
            actionText.gameObject.SetActive(true);
            actionText.text = " 연금 테이블 조작 " + "<color=yellow>" + "(E)" + "</color>";
        }
    }

    private void TrapInfoAppear()
    {
        if (hitInfo.transform.GetComponent<DeadTrap>().GetIsActivated())
        {
            Reset();
            lookActivatedTrap = true;
            actionText.gameObject.SetActive(true);
            actionText.text = " 함정 재설치 " + "<color=yellow>" + "(E)" + "</color>";
        }
    }

    private void NpcInfoAppear()
    {
        if (hitInfo.transform.GetComponent<NPC>() != null)
        {
            Reset();
            lookNpc = true;
            actionText.gameObject.SetActive(true);
            actionText.text = hitInfo.transform.name + "에게 말 걸기" + "<color=yellow>" + "(E)" + "</color>";
        }
    }

    private void ItemInfoDisappear()
    {
        pickupActivate = false;
        lookComputer = false;
        lookNpc = false;
        lookArchemyTable = false;
        lookActivatedTrap = false;
        actionText.gameObject.SetActive(false);
    }

    private void Reset()
    {
        pickupActivate = false;
    }

}
