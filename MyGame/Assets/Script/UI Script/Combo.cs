﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Combo : MonoBehaviour
{
    // 필요 컴포넌트
    private PlayerController playerController = null;

    void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
        attackCount = 0;
        isAttack = false;
    }

    private void OnEnable()
    {
        attackCount = 0;
        isAttack = false;
    }

    public Slider slider;
    public float speed;

    public float minPos;
    public float maxPos;
    public RectTransform pass;
    public int attackCount;

    private bool isAttack = false;
    private bool isCombo = false;

    public void SetAttack()
    {
        slider.value = 0;
        minPos = pass.anchoredPosition.x;
        maxPos = pass.sizeDelta.x + minPos;
        StartCoroutine(ComboAttack());
    }

    public void ResetUI()
    {
        slider.value = 0;
        isAttack = false;
        minPos = pass.anchoredPosition.x;
        maxPos = pass.sizeDelta.x + minPos;
        attackCount = 0;
    }

    public void ComboUIActivate(bool isActivate)
    {
        gameObject.SetActive(isActivate);
        ResetUI();
    }

    public void UIUpdate()
    {
        if (!isAttack)
        {
            isAttack = true;
            isCombo = false;
            SetAttack();
        }
    }

    IEnumerator ComboAttack()
    {
        yield return null;

        while (!(Input.GetButtonDown("Fire1") || slider.value == slider.maxValue))
        {
            slider.value += Time.deltaTime * speed;
            yield return null;
        }

        if (slider.value > minPos && slider.value < maxPos)
        {
            attackCount++;
            if (attackCount < 3)
            {
                switch (attackCount)
                {
                    case 1:     // 두번째 어택 실행
                        playerController.AnimatorSetTrigger("Attack2");
                        break;
                    case 2:     // 세번째 어택 실행
                        playerController.AnimatorSetTrigger("Attack3");
                        break;
                }
            }
            else
            {
                playerController.AnimatorSetTrigger("Attack1");
                attackCount = 0;
            }
            isCombo = true;
            SetAttack();
        }
        else  // 바가 끝까지 닿았거나 이전에 클릭했을 경우 [ 콤보 실패 ]
        {
            ResetUI();
            attackCount = 0;
            isAttack = false;
            isCombo = false;
            ComboUIActivate(false);
        }
        slider.value = 0;
    }

    //public IEnumerator ComboAttack()
    //{
    //    yield return null;

    //    while (!(Input.GetButtonDown("Fire1") || slider.value == slider.maxValue))
    //    {
    //        slider.value += Time.deltaTime * speed;
    //        yield return null;
    //    }

    //    // 마우스 클릭을 했거나 sliderbar가 전부 가득참

    //    // 콤보박스 안에 실행됨
    //    if (slider.value >= minPos && slider.value <= maxPos)
    //    {
    //        attackCount++;
    //        if (attackCount < 3)
    //        {
    //            switch(attackCount)
    //            {
    //                case 1:     // 두번째 어택 실행
    //                    playerController.AnimatorSetTrigger("Attack2");
    //                    break;
    //                case 2:     // 세번째 어택 실행
    //                    playerController.AnimatorSetTrigger("Attack3");
    //                    break;
    //            }
    //        }
    //        else // 콤보를 다시 처음으로 돌림
    //        {
    //            playerController.AnimatorSetTrigger("Attack1");
    //            attackCount = 0;
    //            isAttack = false;
    //        }
    //        isCombo = true;
    //    }
    //    else  // 입력하지 못했음 [ 공격 종료 ]
    //    {
    //        isCombo = false;
    //        isAttack = false;
    //        attackCount = 0;
    //    }
    //    slider.value = 0;
    //}

    public bool GetIsCombo() { return isCombo; }



}
