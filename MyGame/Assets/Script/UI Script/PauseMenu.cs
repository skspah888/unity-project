﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private GameObject go_BaseUI = null;
    [SerializeField] private SaveNLoad theSaveNLoad = null;



    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            if (!GameManager.isPause)
                CallMenu();
            else
                CloseMenu();
        }
    }

    private void CallMenu()
    {
        GameManager.isPause = true;
        go_BaseUI.SetActive(true);
        Time.timeScale = 0f;
    }

    private void CloseMenu()
    {
        GameManager.isPause = false;
        go_BaseUI.SetActive(false);
        Time.timeScale = 1f;
    }

    public void ClickSave()
    {
        Debug.Log("세이브 버튼 클릭");
        theSaveNLoad.SaveGame();
    }

    public void ClickLoad()
    {
        Debug.Log("로드 버튼 클릭");
        theSaveNLoad.LoadGame();
    }

    public void ClickExit()
    {
        Debug.Log("Exit 버튼 클릭");
        Application.Quit();
    }
}
