﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour
{
    public string SceneName = "Town";

    public static Title instance;

    private SaveNLoad theSaveNLoad;
    [SerializeField] private GameObject thePanel = null;

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(this.gameObject);
    }

    public void ClickStart()
    {
        Debug.Log("게임 스타트");
        SceneManager.LoadSceneAsync(SceneName);
        thePanel.SetActive(false);
    }

    public void ClickLoad()
    {
        Debug.Log("게임 로드");
        StartCoroutine(LoadCoroutine());
        thePanel.SetActive(false);
    }

    IEnumerator LoadCoroutine()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(SceneName);

        // 씬 동기화
        while(!operation.isDone)
        {
            yield return null;
        }

        theSaveNLoad = FindObjectOfType<SaveNLoad>();
        theSaveNLoad.LoadGame();
        gameObject.SetActive(false);
    }

    public void ClickExit()
    {
        Debug.Log("게임 종료");
        Application.Quit();
    }

}
