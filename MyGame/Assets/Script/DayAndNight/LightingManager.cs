﻿using System;
using UnityEngine;

[ExecuteAlways]
public class LightingManager : MonoBehaviour
{
    [SerializeField] private Light DirectionalLight = null;
    [SerializeField] private LightingPreset Preset = null;
    [SerializeField, Range(0, 24)] private float TimeOfDay = 0f;
    [SerializeField] private float CycleSpeed = 1f;
    private float CurrentCycleSpeed = 0f;

    private void Update()
    {
        if (Preset == null)
            return;

        UpdateSpeed();

        if (Application.isPlaying)
        {
            TimeOfDay += Time.deltaTime / CycleSpeed;
            TimeOfDay %= 24; 
            UpdateLighting(TimeOfDay / 24f);
        }
        else
        {
            UpdateLighting(TimeOfDay / 24f);
        }
    }

    private void UpdateSpeed()
    {
        float temp = TimeOfDay %= 24;
        if ((temp > 0 && temp < 6) || temp > 19 && temp < 24)
            CurrentCycleSpeed = CycleSpeed / 3f;
        else
            CurrentCycleSpeed = CycleSpeed;
    }

    private void UpdateLighting(float timePercent)
    {
        // 안개 설정
        RenderSettings.ambientLight = Preset.AmbientColor.Evaluate(timePercent);
        RenderSettings.fogColor = Preset.FogColor.Evaluate(timePercent);

        if (DirectionalLight != null)
        {
            DirectionalLight.color = Preset.DirectionalColor.Evaluate(timePercent);

            DirectionalLight.transform.localRotation = Quaternion.Euler(new Vector3((timePercent * 360f) - 90f, 170f, 0));
        }

    }

    private void OnValidate()
    {
        if (DirectionalLight != null)
            return;

        if (RenderSettings.sun != null)
        {
            DirectionalLight = RenderSettings.sun;
        }
        else
        {
            Light[] lights = GameObject.FindObjectsOfType<Light>();
            foreach (Light light in lights)
            {
                if (light.type == LightType.Directional)
                {
                    DirectionalLight = light;
                    return;
                }
            }
        }
    }

}
