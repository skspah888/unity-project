﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDialogable
{
    // 입력으로 받는 target은 대화를 건 사람
    void NPCActivate();
}
